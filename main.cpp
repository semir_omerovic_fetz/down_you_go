#if defined(UNICODE) && !defined(_UNICODE)
    #define _UNICODE
#elif defined(_UNICODE) && !defined(UNICODE)
    #define UNICODE
#endif

#include <tchar.h>
#include <windows.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <vector>
#include <map>
#include <string>
#include <cstdlib>

using namespace std;
#define keyPressed(vk_code) ((GetAsyncKeyState(vk_code) & 0x8000) ? 1 : 0)

/*  Declare Windows procedure  */
LRESULT CALLBACK WindowProcedure (HWND, UINT, WPARAM, LPARAM);

/*  Make the class name into a global variable  */
TCHAR szClassName[ ] = _T("CodeBlocksWindowsApp");

std::map<string, int> mapHighScores;

static int floor = 70000;
static int mario_to_floor = 0;
static int yoshi_to_floor = 0;
int characterHeight = 120;
int characterWidth = 70;
int timerInterval = 400;

int normalSpeed = 12;
int slowedSpeed = 8;

DWORD startTime;
DWORD marioTime;
DWORD yoshiTime;

HFONT font =  CreateFont(90,40,0,5,FW_HEAVY,1,0,0,MAC_CHARSET,0,0,5,2,NULL);
HFONT font2 =  CreateFont(40,20,0,5,FW_HEAVY,1,0,0,MAC_CHARSET,0,0,5,2,NULL);

bool isMarioWinner = false;

bool onceMario = true;
bool onceYoshi = true;
bool marioSlowed = false;
bool yoshiSlowed = false;
bool marioImortal = false;
bool yoshiImortal = false;
bool marioOnFloor = false;
bool yoshiOnFloor = false;
bool marioGoOnFloor = false;
bool yoshiGoOnFloor = false;

bool isPlaying = false;
bool isMenu = true;
bool isEndGame = false;
bool isHighScores = false;
bool isInstructions = false;

string highScoreNames[5];
string highScoreTimes[5];
string time,name;

void Initialize();
void UpdatePicture(HWND);
void Draw(HWND);
void drawPlaying(HWND);
void drawMenu(HWND);
void drawEndGame(HWND);
void drawHighScores(HWND);
void drawInstructions(HWND);
void CheckInput();
void UpdateCharacters(RECT*);
void UpdateBackgrounds(RECT*);
void UpdateObstacles(RECT*);
void drawObstacle();
void startNewGame();
void showScores();
void showInstructions();
void readFile() {
    int countlines = 0;
    ifstream myReadFile;
    myReadFile.open("highscores.txt");
    string output;
    if (myReadFile.is_open()) {
        while (!myReadFile.eof()) {
            myReadFile >> output;
            std::istringstream ss(output);
            std::string token;
            int counter = 0;
            while(std::getline(ss, token, ',')) {
            if(counter == 0 )
                highScoreNames[countlines] = token;
            else
                highScoreTimes[countlines] = token;
            counter++;
            }
            countlines++;
        }
    }
    myReadFile.close();
}

void writeInFile() {
    ofstream myfile;
    myfile.open ("highscores.txt");
    for(int i=0;i<5;i++) {
        myfile << highScoreNames[i] << "," << highScoreTimes[i];
        if(i<4)
            myfile << endl;
    }
    myfile.close();
}

void slowMario();
void fastMario();
void slowYoshi();
void fastYoshi();

DWORD yoshiFlickerAt;
DWORD marioFlickerAt;

class Object {
    public:
    int type;//0 means obstacle, 1 means speed up, 2 means slow down, 3 means switch

    int width;
    int height;

    int xpos;
    int ypos;
    int dx;
    int dy;
    bool isMario;
    bool collidedOnce;
};

bool pointInsideObject(int, int, Object);

DWORD marioCollision = -1;
DWORD yoshiCollision = -1;
DWORD marioSpedUp = -1;
DWORD yoshiSpedUp = -1;
DWORD marioSlowDown = -1;
DWORD yoshiSlowDown = -1;

std::vector<Object> obstacles;
void checkForCollisions(HWND);
BOOL overlapping(Object,Object);

Object background, background2;
Object newGame, instructions, scores, quit, playAgain, gotoMainMenu, goBack;
Object chain;
Object chain2;
Object yoshi, mario;

static int explosionWidth;
static int explosionHeight;

HBITMAP hbmBackground,hbmBackgroundFloor, hbmEndGame, hbmHighScore, hbmInstructionsScreen;
HBITMAP hbmChain;
HBITMAP hbmChainMask;
HBITMAP hbmBlue, hbmBlueMask, hbmRed, hbmRedMask, hbmObstacle, hbmObstacleMask;
HBITMAP hbmexplosion;
HBITMAP hbmexplosionblack;
HBITMAP hbmYoshiStand, hbmYoshiStandBlack, hbmMarioStand, hbmMarioStandBlack;

HBITMAP hbmPlay, hbmPlayBlack, hbmInstructions, hbmInstructionsBlack, hbmScores, hbmScoresBlack, hbmExit, hbmExitBlack;
HBITMAP hbmPlayAgain, hbmPlayAgainBlack, hbmGoToMainMenu, hbmGoToMainMenuBlack, hbmGoBack, hbmGoBackBlack;

HBITMAP hbmSpeed, hbmSpeedBlack, hbmSlow, hbmSlowBlack, hbmSwitch, hbmSwitchBlack;

HBITMAP hbmPlayReg, hbmPlayRegBlack, hbmPlayHover, hbmPlayHoverBlack, hbmInstrReg, hbmInstrRegBlack, hbmInstrHover, hbmInstrHoverBlack, hbmScoresReg, hbmScoresRegBlack, hbmScoresHover, hbmScoresHoverBlack, hbmExitReg, hbmExitRegBlack, hbmExitHover, hbmExitHoverBlack, hbmPlayAgainReg, hbmPlayAgainRegBlack, hbmPlayAgainHover, hbmPlayAgainHoverBlack, hbmGoToMainMenuReg, hbmGoToMainMenuRegBlack, hbmGoToMainMenuHover, hbmGoToMainMenuHoverBlack, hbmGoBackReg, hbmGoBackRegBlack, hbmGoBackHover, hbmGoBackHoverBlack, hbmYoshiSad, hbmYoshiSadBlack, hbmMarioSad, hbmMarioSadBlack, hbmYoshiHappy, hbmYoshiHappyBlack, hbmMarioHappy, hbmMarioHappyBlack, hbmMarioRT, hbmMarioRTBlack, hbmMarioLT, hbmMarioLTBlack, MarioRDizzy, MarioRDizzyBlack, MarioLDizzy, MarioLDizzyBlack, MarioR, MarioRBlack, MarioL, MarioLBlack, YoshiLT, YoshiLTBlack, YoshiRT, YoshiRTBlack, YoshiLDizzy, YoshiLDizzyBlack, YoshiRDizzy, YoshiRDizzyBlack, YoshiL, YoshiLBlack, YoshiR, YoshiRBlack ;

int WINAPI WinMain (HINSTANCE hThisInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR lpszArgument,
                     int nCmdShow)
{
    HWND hwnd;               /* This is the handle for our window */
    MSG messages;            /* Here messages to the application are saved */
    WNDCLASSEX wincl;        /* Data structure for the windowclass */

    /* The Window structure */
    wincl.hInstance = hThisInstance;
    wincl.lpszClassName = szClassName;
    wincl.lpfnWndProc = WindowProcedure;      /* This function is called by windows */
    wincl.style = CS_DBLCLKS;                 /* Catch double-clicks */
    wincl.cbSize = sizeof (WNDCLASSEX);

    /* Use default icon and mouse-pointer */
    wincl.hIcon = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hIconSm = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hCursor = LoadCursor (NULL, IDC_ARROW);
    wincl.lpszMenuName = NULL;                 /* No menu */
    wincl.cbClsExtra = 0;                      /* No extra bytes after the window class */
    wincl.cbWndExtra = 0;                      /* structure or the window instance */
    /* Use Windows's default colour as the background of the window */
    wincl.hbrBackground = (HBRUSH) COLOR_BACKGROUND;

    /* Register the window class, and if it fails quit the program */
    if (!RegisterClassEx (&wincl))
        return 0;

    /* The class is registered, let's create the program*/
    hwnd = CreateWindowEx (
           0,                   /* Extended possibilites for variation */
           szClassName,         /* Classname */
           _T("Down you go"),       /* Title Text */
           WS_OVERLAPPED | WS_MINIMIZEBOX | WS_SYSMENU, /* default window */
           250,       /* Windows decides the position */
           10,       /* where the window ends up on the screen */
           900,                 /* The programs width */
           800,                 /* and height in pixels */
           HWND_DESKTOP,        /* The window is a child-window to desktop */
           NULL,                /* No menu */
           hThisInstance,       /* Program Instance handler */
           NULL                 /* No Window Creation data */
           );

    /* Make the window visible on the screen */
    ShowWindow (hwnd, nCmdShow);
    readFile();
    Initialize();

    while(TRUE)
    {
        DWORD start;
        if(PeekMessage(&messages, NULL, 0, 0, PM_REMOVE))
        {
            if(messages.message == WM_QUIT)
            {
                break;
            }
            TranslateMessage(&messages);
            DispatchMessage(&messages);
        }

        start = GetTickCount();
        CheckInput();
        UpdatePicture(hwnd);
        Draw(hwnd);

        while((GetTickCount() - start) < 15)
        {
            Sleep(5);
        }
    }

    /* The program return-value is 0 - The value that PostQuitMessage() gave */
    return messages.wParam;
}


/*  This function is called by the Windows function DispatchMessage()  */

LRESULT CALLBACK WindowProcedure (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)                  /* handle the messages */
    {
        case WM_CREATE:
            {
            SetTimer(hwnd, 0, timerInterval, NULL);
            break;
            }

        case WM_MOUSEMOVE:
        {
            int x = LOWORD(lParam);
            int y = HIWORD(lParam);
            if(isMenu){
                if(pointInsideObject(x,y,newGame)) {
                     hbmPlay = hbmPlayHover;
                     hbmPlayBlack = hbmPlayHoverBlack;
                }
                else {
                    hbmPlay = hbmPlayReg;
                    hbmPlayBlack = hbmPlayRegBlack;
                }
                if(pointInsideObject(x,y,instructions)) {
                     hbmInstructions = hbmInstrHover;
                     hbmInstructionsBlack = hbmInstrHoverBlack;
                }
                else {
                    hbmInstructions = hbmInstrReg;
                    hbmInstructionsBlack = hbmInstrRegBlack;
                }
                if(pointInsideObject(x,y,scores)) {
                     hbmScores = hbmScoresHover;
                     hbmScoresBlack = hbmScoresHoverBlack;
                }
                else {
                    hbmScores = hbmScoresReg;
                    hbmScoresBlack = hbmScoresRegBlack;
                }
                if(pointInsideObject(x,y,quit)) {
                     hbmExit = hbmExitHover;
                     hbmExitBlack = hbmExitHoverBlack;
                }
                else {
                    hbmExit = hbmExitReg;
                    hbmExitBlack = hbmExitRegBlack;
                }
            }
            else if(isEndGame) {
                if(pointInsideObject(x,y,playAgain)) {
                     hbmPlayAgain = hbmPlayAgainHover;
                     hbmPlayAgainBlack = hbmPlayAgainHoverBlack;
                }
                else {
                    hbmPlayAgain = hbmPlayAgainReg;
                    hbmPlayAgainBlack = hbmPlayAgainRegBlack;
                }
                if(pointInsideObject(x,y,gotoMainMenu)) {
                     hbmGoToMainMenu = hbmGoToMainMenuHover;
                     hbmGoToMainMenuBlack = hbmGoToMainMenuHoverBlack;
                }
                else {
                    hbmGoToMainMenu = hbmGoToMainMenuReg;
                    hbmGoToMainMenuBlack = hbmGoToMainMenuRegBlack;
                }
            }
            else if(isHighScores) {
                if(pointInsideObject(x,y,goBack)) {
                     hbmGoBack = hbmGoBackHover;
                     hbmGoBackBlack = hbmGoBackHoverBlack;
                }
                else {
                    hbmGoBack = hbmGoBackReg;
                    hbmGoBackBlack = hbmGoBackRegBlack;
                }
            }
            else if(isInstructions) {
                if(pointInsideObject(x,y,goBack)) {
                     hbmGoBack = hbmGoBackHover;
                     hbmGoBackBlack = hbmGoBackHoverBlack;
                }
                else {
                    hbmGoBack = hbmGoBackReg;
                    hbmGoBackBlack = hbmGoBackRegBlack;
                }
            }
            break;
        }
        case WM_LBUTTONDOWN:
            {
                int x = LOWORD(lParam);
                int y = HIWORD(lParam);
                if(isMenu){
                    if(pointInsideObject(x,y,newGame)) {
                        startNewGame();
                    }
                    else if(pointInsideObject(x,y,instructions)) {
                        showInstructions();
                    }
                    else if(pointInsideObject(x,y,scores)) {
                        showScores();
                    }
                    else if(pointInsideObject(x,y,quit)) {
                        PostQuitMessage (0);
                    }
                }
                else if(isEndGame) {
                    if(pointInsideObject(x,y,playAgain)) {
                        if(isMarioWinner)
                            mciSendString("play sound/mario_taunt.wav", NULL, 0, NULL);
                        else
                            mciSendString("play sound/yoshi_taunt.wav", NULL, 0, NULL);
                        startNewGame();
                    }
                    else if(pointInsideObject(x,y,gotoMainMenu)) {
                        isPlaying = false;
                        isEndGame = false;
                        isHighScores = false;
                        isInstructions = false;
                        isMenu = true;
                    }
                }
                else if(isHighScores) {
                    if(pointInsideObject(x,y,goBack)) {
                        hbmGoBack = hbmGoBackReg;
                        hbmGoBackBlack = hbmGoBackRegBlack;
                        isPlaying = false;
                        isEndGame = false;
                        isHighScores = false;
                        isInstructions = false;
                        isMenu = true;
                        }
                }
                else if(isInstructions) {
                    if(pointInsideObject(x,y,goBack)) {
                        hbmGoBack = hbmGoBackReg;
                        hbmGoBackBlack = hbmGoBackRegBlack;
                        isPlaying = false;
                        isEndGame = false;
                        isHighScores = false;
                        isInstructions = false;
                        isMenu = true;
                        }
                }

            break;
            }
        case WM_TIMER:
            drawObstacle();
            break;
        case WM_DESTROY:
            PostQuitMessage (0);       /* send a WM_QUIT to the message queue */
            break;
        default:                      /* for messages that we don't deal with */
            return DefWindowProc (hwnd, message, wParam, lParam);
    }

    return 0;
}

void Initialize()
{
    BITMAP bitmap;

    hbmBackground= (HBITMAP)LoadImage(NULL, "img/back.bmp", IMAGE_BITMAP, 900, 900, LR_LOADFROMFILE);
    hbmBackgroundFloor= (HBITMAP)LoadImage(NULL, "img/floor.bmp", IMAGE_BITMAP, 900, 820, LR_LOADFROMFILE);
    hbmChainMask = (HBITMAP)LoadImage(NULL, "img/chain_black.bmp", IMAGE_BITMAP, 300, 1000, LR_LOADFROMFILE);
    hbmChain = (HBITMAP)LoadImage(NULL, "img/chain.bmp", IMAGE_BITMAP, 300, 1000, LR_LOADFROMFILE);

    hbmEndGame = (HBITMAP)LoadImage(NULL, "img/endgame.bmp", IMAGE_BITMAP, 900, 900, LR_LOADFROMFILE);
    hbmPlay = (HBITMAP)LoadImage(NULL, "img/play.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmPlayBlack = (HBITMAP)LoadImage(NULL, "img/play_black.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmInstructions = (HBITMAP)LoadImage(NULL, "img/instructions.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmInstructionsBlack = (HBITMAP)LoadImage(NULL, "img/instructions_black.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmScores = (HBITMAP)LoadImage(NULL, "img/scores.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmScoresBlack = (HBITMAP)LoadImage(NULL, "img/scores_black.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmExit = (HBITMAP)LoadImage(NULL, "img/exit.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmExitBlack = (HBITMAP)LoadImage(NULL, "img/exit_black.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmPlayAgain = (HBITMAP)LoadImage(NULL, "img/playagain.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmPlayAgainBlack = (HBITMAP)LoadImage(NULL, "img/playagain_black.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmGoToMainMenu = (HBITMAP)LoadImage(NULL, "img/gotomainmenu.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmGoToMainMenuBlack = (HBITMAP)LoadImage(NULL, "img/gotomainmenu_black.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmGoBack = (HBITMAP)LoadImage(NULL, "img/go_back.bmp", IMAGE_BITMAP, 200, 115, LR_LOADFROMFILE);
    hbmGoBackBlack = (HBITMAP)LoadImage(NULL, "img/go_back_black.bmp", IMAGE_BITMAP, 200, 115, LR_LOADFROMFILE);
    hbmHighScore = (HBITMAP)LoadImage(NULL, "img/highscore.bmp", IMAGE_BITMAP, 900, 900, LR_LOADFROMFILE);
    hbmInstructionsScreen = (HBITMAP)LoadImage(NULL, "img/instructionsScreen.bmp", IMAGE_BITMAP, 900, 775, LR_LOADFROMFILE);

    hbmBlue = (HBITMAP)LoadImage(NULL, "img/yoshi_left.bmp", IMAGE_BITMAP, characterWidth, characterHeight, LR_LOADFROMFILE);
    hbmBlueMask = (HBITMAP)LoadImage(NULL, "img/yoshi_left_black.bmp", IMAGE_BITMAP, characterWidth, characterHeight, LR_LOADFROMFILE);
    hbmRed = (HBITMAP)LoadImage(NULL, "img/mario_left.bmp", IMAGE_BITMAP, characterWidth, characterHeight, LR_LOADFROMFILE);
    hbmRedMask = (HBITMAP)LoadImage(NULL, "img/mario_left_black.bmp", IMAGE_BITMAP, characterWidth, characterHeight, LR_LOADFROMFILE);

    hbmexplosion = (HBITMAP)LoadImage(NULL, "img/explosion.bmp", IMAGE_BITMAP, 600, 600, LR_LOADFROMFILE);
    hbmexplosionblack = (HBITMAP)LoadImage(NULL, "img/explosion_black.bmp", IMAGE_BITMAP, 600, 600, LR_LOADFROMFILE);

    hbmObstacle = (HBITMAP)LoadImage(NULL, "img/spiked_ball.bmp", IMAGE_BITMAP, 100, 100, LR_LOADFROMFILE);
    hbmObstacleMask = (HBITMAP)LoadImage(NULL, "img/spiked_ball_black.bmp", IMAGE_BITMAP, 100, 100, LR_LOADFROMFILE);
    hbmSpeed = (HBITMAP)LoadImage(NULL, "img/speedup.bmp", IMAGE_BITMAP, 50, 100, LR_LOADFROMFILE);
    hbmSpeedBlack = (HBITMAP)LoadImage(NULL, "img/speedup_black.bmp", IMAGE_BITMAP, 50, 100, LR_LOADFROMFILE);
    hbmSlow = (HBITMAP)LoadImage(NULL, "img/slowdown.bmp", IMAGE_BITMAP, 50, 100, LR_LOADFROMFILE);
    hbmSlowBlack = (HBITMAP)LoadImage(NULL, "img/slowdown_black.bmp", IMAGE_BITMAP, 50, 100, LR_LOADFROMFILE);
    hbmSwitch = (HBITMAP)LoadImage(NULL, "img/switch.bmp", IMAGE_BITMAP, 50, 100, LR_LOADFROMFILE);
    hbmSwitchBlack = (HBITMAP)LoadImage(NULL, "img/switch_black.bmp", IMAGE_BITMAP, 50, 100, LR_LOADFROMFILE);

    hbmPlayReg = (HBITMAP)LoadImage(NULL, "img/play.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmPlayRegBlack = (HBITMAP)LoadImage(NULL, "img/play_black.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmPlayHover = (HBITMAP)LoadImage(NULL, "img/play_hover.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmPlayHoverBlack = (HBITMAP)LoadImage(NULL, "img/play_hover_black.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmInstrReg = (HBITMAP)LoadImage(NULL, "img/instructions.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmInstrRegBlack = (HBITMAP)LoadImage(NULL, "img/instructions_black.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmInstrHover = (HBITMAP)LoadImage(NULL, "img/instructions_hover.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmInstrHoverBlack = (HBITMAP)LoadImage(NULL, "img/instructions_hover_black.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmScoresReg = (HBITMAP)LoadImage(NULL, "img/scores.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmScoresRegBlack = (HBITMAP)LoadImage(NULL, "img/scores_black.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmScoresHover = (HBITMAP)LoadImage(NULL, "img/scores_hover.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmScoresHoverBlack = (HBITMAP)LoadImage(NULL, "img/scores_hover_black.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmExitReg = (HBITMAP)LoadImage(NULL, "img/exit.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmExitRegBlack = (HBITMAP)LoadImage(NULL, "img/exit_black.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmExitHover = (HBITMAP)LoadImage(NULL, "img/exit_hover.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmExitHoverBlack = (HBITMAP)LoadImage(NULL, "img/exit_hover_black.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmPlayAgainReg = (HBITMAP)LoadImage(NULL, "img/playagain.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmPlayAgainRegBlack = (HBITMAP)LoadImage(NULL, "img/playagain_black.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmPlayAgainHover = (HBITMAP)LoadImage(NULL, "img/playagain_hover.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmPlayAgainHoverBlack = (HBITMAP)LoadImage(NULL, "img/playagain_hover_black.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmGoToMainMenuReg = (HBITMAP)LoadImage(NULL, "img/gotomainmenu.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmGoToMainMenuRegBlack = (HBITMAP)LoadImage(NULL, "img/gotomainmenu_black.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmGoToMainMenuHover = (HBITMAP)LoadImage(NULL, "img/gotomainmenu_hover.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmGoToMainMenuHoverBlack = (HBITMAP)LoadImage(NULL, "img/gotomainmenu_hover_black.bmp", IMAGE_BITMAP, 378, 115, LR_LOADFROMFILE);
    hbmGoBackReg = (HBITMAP)LoadImage(NULL, "img/go_back.bmp", IMAGE_BITMAP, 200, 115, LR_LOADFROMFILE);
    hbmGoBackRegBlack = (HBITMAP)LoadImage(NULL, "img/go_back_black.bmp", IMAGE_BITMAP, 200, 115, LR_LOADFROMFILE);
    hbmGoBackHover = (HBITMAP)LoadImage(NULL, "img/go_back_hover.bmp", IMAGE_BITMAP, 200, 115, LR_LOADFROMFILE);
    hbmGoBackHoverBlack = (HBITMAP)LoadImage(NULL, "img/go_back_hover_black.bmp", IMAGE_BITMAP, 200, 115, LR_LOADFROMFILE);
    hbmYoshiSad = (HBITMAP)LoadImage(NULL, "img/yoshi_sad.bmp", IMAGE_BITMAP, 300, 400, LR_LOADFROMFILE);
    hbmYoshiSadBlack = (HBITMAP)LoadImage(NULL, "img/yoshi_sad_black.bmp", IMAGE_BITMAP, 300, 400, LR_LOADFROMFILE);
    hbmMarioSad = (HBITMAP)LoadImage(NULL, "img/mario_sad.bmp", IMAGE_BITMAP, 300, 400, LR_LOADFROMFILE);
    hbmMarioSadBlack = (HBITMAP)LoadImage(NULL, "img/mario_sad_black.bmp", IMAGE_BITMAP, 300, 400, LR_LOADFROMFILE);
    hbmYoshiHappy = (HBITMAP)LoadImage(NULL, "img/yoshi_happy.bmp", IMAGE_BITMAP, 300, 400, LR_LOADFROMFILE);
    hbmYoshiHappyBlack = (HBITMAP)LoadImage(NULL, "img/yoshi_happy_black.bmp", IMAGE_BITMAP, 300, 400, LR_LOADFROMFILE);
    hbmMarioHappy = (HBITMAP)LoadImage(NULL, "img/mario_happy.bmp", IMAGE_BITMAP, 300, 400, LR_LOADFROMFILE);
    hbmMarioHappyBlack = (HBITMAP)LoadImage(NULL, "img/mario_happy_black.bmp", IMAGE_BITMAP, 300, 400, LR_LOADFROMFILE);
    hbmMarioRT = (HBITMAP)LoadImage(NULL, "img/mario_right_transparent.bmp", IMAGE_BITMAP, characterWidth, characterHeight, LR_LOADFROMFILE);
    hbmMarioRTBlack = (HBITMAP)LoadImage(NULL, "img/mario_right_black_transparent.bmp", IMAGE_BITMAP, characterWidth, characterHeight, LR_LOADFROMFILE);
    hbmMarioLT = (HBITMAP)LoadImage(NULL, "img/mario_left_transparent.bmp", IMAGE_BITMAP, characterWidth, characterHeight, LR_LOADFROMFILE);
    hbmMarioLTBlack = (HBITMAP)LoadImage(NULL, "img/mario_left_black_transparent.bmp", IMAGE_BITMAP, characterWidth, characterHeight, LR_LOADFROMFILE);
    MarioRDizzy = (HBITMAP)LoadImage(NULL, "img/mario_right_dizzy.bmp", IMAGE_BITMAP, characterWidth, characterHeight, LR_LOADFROMFILE);
    MarioRDizzyBlack = (HBITMAP)LoadImage(NULL, "img/mario_right_black_dizzy.bmp", IMAGE_BITMAP, characterWidth, characterHeight, LR_LOADFROMFILE);
    MarioLDizzy = (HBITMAP)LoadImage(NULL, "img/mario_left_dizzy.bmp", IMAGE_BITMAP, characterWidth, characterHeight, LR_LOADFROMFILE);
    MarioLDizzyBlack = (HBITMAP)LoadImage(NULL, "img/mario_left_black_dizzy.bmp", IMAGE_BITMAP, characterWidth, characterHeight, LR_LOADFROMFILE);
    MarioR = (HBITMAP)LoadImage(NULL, "img/mario_right.bmp", IMAGE_BITMAP, characterWidth, characterHeight, LR_LOADFROMFILE);
    MarioRBlack = (HBITMAP)LoadImage(NULL, "img/mario_right_black.bmp", IMAGE_BITMAP, characterWidth, characterHeight, LR_LOADFROMFILE);
    MarioL = (HBITMAP)LoadImage(NULL, "img/mario_left.bmp", IMAGE_BITMAP, characterWidth, characterHeight, LR_LOADFROMFILE);
    MarioLBlack = (HBITMAP)LoadImage(NULL, "img/mario_left_black.bmp", IMAGE_BITMAP, characterWidth, characterHeight, LR_LOADFROMFILE);
    YoshiLT = (HBITMAP)LoadImage(NULL, "img/yoshi_left_transparent.bmp", IMAGE_BITMAP, characterWidth, characterHeight, LR_LOADFROMFILE);
    YoshiLTBlack = (HBITMAP)LoadImage(NULL, "img/yoshi_left_black_transparent.bmp", IMAGE_BITMAP, characterWidth, characterHeight, LR_LOADFROMFILE);
    YoshiRT = (HBITMAP)LoadImage(NULL, "img/yoshi_right_transparent.bmp", IMAGE_BITMAP, characterWidth, characterHeight, LR_LOADFROMFILE);
    YoshiRTBlack = (HBITMAP)LoadImage(NULL, "img/yoshi_right_black_transparent.bmp", IMAGE_BITMAP, characterWidth, characterHeight, LR_LOADFROMFILE);
    YoshiLDizzy = (HBITMAP)LoadImage(NULL, "img/yoshi_left_dizzy.bmp", IMAGE_BITMAP, characterWidth, characterHeight, LR_LOADFROMFILE);
    YoshiLDizzyBlack = (HBITMAP)LoadImage(NULL, "img/yoshi_left_black_dizzy.bmp", IMAGE_BITMAP, characterWidth, characterHeight, LR_LOADFROMFILE);
    YoshiRDizzy = (HBITMAP)LoadImage(NULL, "img/yoshi_right_dizzy.bmp", IMAGE_BITMAP, characterWidth, characterHeight, LR_LOADFROMFILE);
    YoshiRDizzyBlack = (HBITMAP)LoadImage(NULL, "img/yoshi_right_black_dizzy.bmp", IMAGE_BITMAP, characterWidth, characterHeight, LR_LOADFROMFILE);
    YoshiL = (HBITMAP)LoadImage(NULL, "img/yoshi_left.bmp", IMAGE_BITMAP, characterWidth, characterHeight, LR_LOADFROMFILE);
    YoshiLBlack = (HBITMAP)LoadImage(NULL, "img/yoshi_left_black.bmp", IMAGE_BITMAP, characterWidth, characterHeight, LR_LOADFROMFILE);
    YoshiR = (HBITMAP)LoadImage(NULL, "img/yoshi_right.bmp", IMAGE_BITMAP, characterWidth, characterHeight, LR_LOADFROMFILE);
    YoshiRBlack = (HBITMAP)LoadImage(NULL, "img/yoshi_right_black.bmp", IMAGE_BITMAP, characterWidth, characterHeight, LR_LOADFROMFILE);

    newGame.xpos = (900-378)/2;
    newGame.ypos = 100;
    newGame.width = 378;
    newGame.height = 115;

    playAgain.xpos = newGame.xpos;
    playAgain.ypos = 20;
    playAgain.width = newGame.width;
    playAgain.height = newGame.height;

    gotoMainMenu.xpos = newGame.xpos;
    gotoMainMenu.ypos = playAgain.ypos + playAgain.height + 25;
    gotoMainMenu.width = newGame.width;
    gotoMainMenu.height = newGame.height;

    goBack.xpos = 70;
    goBack.ypos = 600;
    goBack.width = 200;
    goBack.height = newGame.height;

    instructions.xpos = newGame.xpos;
    instructions.ypos = newGame.ypos + newGame.height + 25;
    instructions.width = newGame.width;
    instructions.height = newGame.height;

    scores.xpos = newGame.xpos;
    scores.ypos = instructions.ypos + instructions.height + 25;
    scores.width = newGame.width;
    scores.height = newGame.height;

    quit.xpos = newGame.xpos;
    quit.ypos = scores.ypos + scores.height + 25;
    quit.width = newGame.width;
    quit.height = newGame.height;

    GetObject(hbmBackground, sizeof(BITMAP), &bitmap);
    background.width = bitmap.bmWidth/2;
    background.height = bitmap.bmHeight;
    background.ypos = 0;
    background.dy = normalSpeed;

    background2.width = bitmap.bmWidth/2;
    background2.height = bitmap.bmHeight;
    background2.ypos = 0;
    background2.dy = normalSpeed;

    GetObject(hbmChain, sizeof(BITMAP), &bitmap);
    chain.width=bitmap.bmWidth/1.5;
    chain.height=bitmap.bmHeight;
    chain.xpos = 85;
    chain.ypos = 1;

    chain2.width=bitmap.bmWidth;
    chain2.height=bitmap.bmHeight;
    chain2.xpos = 525;
    chain2.ypos = 1;

    GetObject(hbmBlue, sizeof(BITMAP), &bitmap);
    yoshi.width = bitmap.bmWidth;
    yoshi.height = bitmap.bmHeight;
    yoshi.xpos = chain.xpos + 125;
    yoshi.ypos = -yoshi.height;
    yoshi.dx = 0;
    yoshi.dy = 2;

    mario.width = bitmap.bmWidth;
    mario.height = bitmap.bmHeight;
    mario.xpos = chain2.xpos + 125;
    mario.ypos = -mario.height;
    mario.dx = 0;
    mario.dy = 2;

    GetObject(hbmexplosion,sizeof(BITMAP),&bitmap);
    explosionWidth=bitmap.bmWidth/5;
    explosionHeight=bitmap.bmHeight/5;

}

void UpdateCharacters(RECT *rect) {
    if(!isPlaying)
        return;
    if(yoshi.ypos < 70)
        yoshi.ypos += yoshi.dy;
    if(mario.ypos < 70)
        mario.ypos += mario.dy;
    if (marioGoOnFloor == true && mario.ypos < 620) {
        if(marioSpedUp != -1)
            mario.ypos += mario.dy + 6;
        else if(marioCollision == -1)
            mario.ypos += mario.dy+3;
        else
            mario.ypos += mario.dy;
    }

    if(mario.ypos >= 620 && yoshi.ypos < 620)
        isMarioWinner = true;

    if(mario.ypos >= 615 && onceMario == true) {
        marioTime = GetTickCount();
        marioTime = marioTime - startTime;
        for (int k=0;k<5;k++) {
            try{
            if (stoi(highScoreTimes[k]) > marioTime){
                if(k<4) {
                for(int l=4;l>k;l--) {
                    highScoreNames[l] = highScoreNames[l-1];
                    highScoreTimes[l] = highScoreTimes[l-1];
                }
                }
                highScoreNames[k] = "Mario";
                highScoreTimes[k] = to_string(marioTime);
                break;
            }
            }
            catch(std::invalid_argument){
            }
        }
        onceMario =false;
    }

    if(yoshi.ypos >= 615 && onceYoshi == true) {
        yoshiTime = GetTickCount();
        yoshiTime = yoshiTime - startTime;
        for (int k=0;k<5;k++) {
            try {
            if (stoi(highScoreTimes[k]) > yoshiTime){
                if(k<4) {
                for(int l=4;l>k;l--) {
                    highScoreNames[l] = highScoreNames[l-1];
                    highScoreTimes[l] = highScoreTimes[l-1];
                }
                }
                highScoreNames[k] = "Yoshi";
                highScoreTimes[k] = to_string(yoshiTime);
                break;
            }
            }
            catch(std::invalid_argument){
            }
        }
        onceYoshi = false;
    }

    if (yoshiGoOnFloor == true && yoshi.ypos < 620) {
        if(yoshiSpedUp != -1)
            yoshi.ypos += yoshi.dy + 6;
        else if(yoshiCollision == -1)
            yoshi.ypos += yoshi.dy+3;
        else
            yoshi.ypos += yoshi.dy;
    }

    if(marioGoOnFloor == true && yoshiGoOnFloor == true && mario.ypos >=620 && yoshi.ypos >=620) {
        mciSendString("stop sound/theme_music.wav", NULL, 0, NULL);
        if(isMarioWinner)
            mciSendString("play sound/mario_winner.wav", NULL, 0, NULL);
        else
            mciSendString("play sound/yoshi_winner.wav", NULL, 0, NULL);
        isEndGame = true;
        isPlaying = false;
        isMenu = false;
        isHighScores = false;
        isInstructions = false;
        if(isMarioWinner) {
            hbmYoshiStand = hbmYoshiSad;
            hbmYoshiStandBlack = hbmYoshiSadBlack;
            hbmMarioStand = hbmMarioHappy;
            hbmMarioStandBlack = hbmMarioHappyBlack;
        }
        else {
            hbmYoshiStand = hbmYoshiHappy;
            hbmYoshiStandBlack = hbmYoshiHappyBlack;
            hbmMarioStand = hbmMarioSad;
            hbmMarioStandBlack = hbmMarioSadBlack;
        }
    }
}

void UpdateBackgrounds(RECT *rect) {
    if(!isPlaying)
        return;
    background.ypos += background.dy;
    yoshi_to_floor += background.dy;
    if(background.ypos >= 900) {
        background.ypos -= 900;
    }

    background2.ypos += background2.dy;
    mario_to_floor += background2.dy;
    if(background2.ypos >= 900) {
        background2.ypos -= 900;
    }

    if (mario_to_floor >= floor) {
        marioOnFloor = true ;
    }

    if (yoshi_to_floor >= floor) {
        yoshiOnFloor = true;
    }

    if(marioSpedUp != -1) {
        if(GetTickCount() - marioSpedUp >= 1000) {
            marioSpedUp = -1;
            background2.dy = normalSpeed;
            for(auto it = obstacles.begin(); it != obstacles.end(); it++)
                if(it->isMario)
                    it->dy = normalSpeed;
        }
        else {
            background2.dy = normalSpeed+4;
            for(auto it = obstacles.begin(); it != obstacles.end(); it++)
                if(it->isMario)
                    it->dy = normalSpeed+4;
        }
    }
    if(marioSlowDown != -1) {
        if(GetTickCount() - marioSlowDown >= 1000) {
            marioSlowDown = -1;
            background2.dy = normalSpeed;
            for(auto it = obstacles.begin(); it != obstacles.end(); it++)
                if(it->isMario)
                    it->dy = normalSpeed;
        }
        else {
            background2.dy = normalSpeed-4;
            for(auto it = obstacles.begin(); it != obstacles.end(); it++)
                if(it->isMario)
                    it->dy = normalSpeed-4;
        }
    }
    if(marioCollision != -1) {
        if(GetTickCount() - marioCollision >= 1000){
            fastMario();
            marioImortal = false;
        }
        else{
            slowMario();
            marioImortal = true;
        }
    }
    if(yoshiSpedUp != -1) {
        if(GetTickCount() - yoshiSpedUp >= 1000) {
            yoshiSpedUp = -1;
            background.dy = normalSpeed;
            for(auto it = obstacles.begin(); it != obstacles.end(); it++)
                if(!it->isMario)
                    it->dy = normalSpeed;
        }
        else {
            background.dy = normalSpeed+4;
            for(auto it = obstacles.begin(); it != obstacles.end(); it++)
                if(!it->isMario)
                    it->dy = normalSpeed+4;
        }
    }
    if(yoshiSlowDown != -1) {
        if(GetTickCount() - yoshiSlowDown >= 1000) {
            yoshiSlowDown = -1;
            background.dy = normalSpeed;
            for(auto it = obstacles.begin(); it != obstacles.end(); it++)
                if(!it->isMario)
                    it->dy = normalSpeed;
        }
        else {
            background.dy = normalSpeed-4;
            for(auto it = obstacles.begin(); it != obstacles.end(); it++)
                if(!it->isMario)
                    it->dy = normalSpeed-4;
        }
    }
    if(yoshiCollision != -1) {
        if(GetTickCount() - yoshiCollision >= 1000){
            fastYoshi();
            yoshiImortal = false;
        }
        else {
            slowYoshi();
            yoshiImortal = true;
        }
    }
}

void slowMario() {
    if(!isPlaying)
        return;
    background2.dy = slowedSpeed;
    for(auto it = obstacles.begin(); it != obstacles.end(); it++)
        if(it->isMario)
            it->dy = slowedSpeed;
    if(GetTickCount() - marioFlickerAt > 100) {
    if(!marioSlowed) {
        if(chain2.xpos + 100 == mario.xpos) {
            hbmRed = hbmMarioRT;
            hbmRedMask = hbmMarioRTBlack;
        }
        else {
            hbmRed = hbmMarioLT;
            hbmRedMask = hbmMarioLTBlack;
        }
        marioFlickerAt = GetTickCount();
        marioSlowed = true;
    }
    else {
        if(chain2.xpos + 100 == mario.xpos) {
            hbmRed = MarioRDizzy;
            hbmRedMask = MarioRDizzyBlack;
        }
        else{
            hbmRed = MarioLDizzy;
            hbmRedMask = MarioLDizzyBlack;
        }
        marioFlickerAt = GetTickCount();
        marioSlowed = false;
    }
    }
}

void fastMario() {
    if(!isPlaying)
        return;
    marioSlowed = false;
    marioCollision = -1;
    background2.dy = normalSpeed;
    for(auto it = obstacles.begin(); it != obstacles.end(); it++)
        if(it->isMario)
            it->dy = normalSpeed;
    if(chain2.xpos + 100 == mario.xpos) {
        hbmRed = MarioR;
        hbmRedMask = MarioRBlack;
    }
    else{
        hbmRed = MarioL;
        hbmRedMask = MarioLBlack;
    }
}
void slowYoshi() {
    if(!isPlaying)
        return;
    background.dy = slowedSpeed;
    for(auto it = obstacles.begin(); it != obstacles.end(); it++)
        if(!(it->isMario))
            it->dy = slowedSpeed;
    if(GetTickCount() - yoshiFlickerAt > 100) {
    if(!yoshiSlowed) {
        if(chain.xpos + 125 == yoshi.xpos) {
            hbmBlue = YoshiLT;
            hbmBlueMask = YoshiLTBlack;
        }
        else {
            hbmBlue = YoshiRT;
            hbmBlueMask = YoshiRTBlack;
        }
        yoshiFlickerAt = GetTickCount();
        yoshiSlowed = true;
    }
    else {
        if(chain.xpos + 125 == yoshi.xpos) {
        hbmBlue = YoshiLDizzy;
        hbmBlueMask = YoshiLDizzyBlack;
        }
        else {
            hbmBlue = YoshiRDizzy;
            hbmBlueMask = YoshiRDizzyBlack;
        }
        yoshiFlickerAt = GetTickCount();
        yoshiSlowed = false;
    }
    }
}

void fastYoshi() {
    if(!isPlaying)
        return;
    yoshiSlowed = false;
    yoshiCollision = -1;
    background.dy = normalSpeed;
    for(auto it = obstacles.begin(); it != obstacles.end(); it++)
        if(!(it->isMario))
            it->dy = normalSpeed;

    if(chain.xpos + 125 == yoshi.xpos) {
        hbmBlue = YoshiL;
        hbmBlueMask = YoshiLBlack;

    }
    else {
        hbmBlue = YoshiR;
        hbmBlueMask = YoshiRBlack;
    }
}



void UpdatePicture(HWND hwnd)
{
    if(!isPlaying)
        return;
    RECT clientRectangle;
    GetClientRect(hwnd, &clientRectangle);
    UpdateCharacters(&clientRectangle);
    UpdateBackgrounds(&clientRectangle);
    checkForCollisions(hwnd);
    UpdateObstacles(&clientRectangle);

    if (marioOnFloor == true && background2.ypos >= 900 - normalSpeed) {
        background2.dy = 0;
        marioGoOnFloor = true;
    }
    if (yoshiOnFloor == true && background.ypos >= 900 - normalSpeed) {
        background.dy = 0;
        yoshiGoOnFloor = true;
    }

    if (yoshiOnFloor == true && background.ypos >= 850)
        chain.height = 710;
     if (marioOnFloor == true && background2.ypos >= 850)
        chain2.height = 710;
}

void checkForCollisions(HWND hwnd) {
    if(!isPlaying)
        return;
    for(auto it=obstacles.begin(); it!=obstacles.end(); it++) {
        if(it->collidedOnce)
            continue;
        if(overlapping(*it, mario)) {
            if(!marioImortal && it->type == 0) {
                marioCollision = GetTickCount();
                mciSendString("play sound/mario_hurt.wav", NULL, 0, NULL);
            }
            else if(it->type == 1) {
                marioSpedUp = GetTickCount();
                it->ypos = -500;
            }
            else if(it->type == 2) {
                marioSlowDown = GetTickCount();
                it->ypos = -500;
            }
            else if(it->type == 3) {
                for(auto it2 = obstacles.begin(); it2 != obstacles.end(); it2++) {
                    if(it2->isMario && it2->type == 0) {
                        if(it2->xpos - chain2.xpos == 20)
                            it2->xpos = chain2.xpos + 170;
                        else
                            it2->xpos = chain2.xpos + 20;
                    }
                }
                it->ypos = -500;
            }
            it->collidedOnce = true;
        }
        else if(overlapping(*it, yoshi)) {
            if(!yoshiImortal && it->type == 0) {
                it->collidedOnce = true;
                yoshiCollision = GetTickCount();
                mciSendString("play sound/yoshi_hurt.wav", NULL, 0, NULL);
            }
            else if(it->type == 1) {
                yoshiSpedUp = GetTickCount();
                it->ypos = -500;
            }
            else if(it->type == 2) {
                yoshiSlowDown = GetTickCount();
                it->ypos = -500;
            }
            else if(it->type == 3) {
                for(auto it2 = obstacles.begin(); it2 != obstacles.end(); it2++) {
                    if(!it2->isMario && it2->type == 0) {
                        if(it2->xpos - chain.xpos == 20)
                            it2->xpos = chain.xpos + 170;
                        else
                            it2->xpos = chain.xpos + 20;
                    }
                }
                it->ypos = -500;
            }
            it->collidedOnce = true;
        }
    }
}

BOOL overlapping(Object obstacle, Object character) {
    if(!isPlaying)
        return false;
    int xBegin, xEnd, yBegin, yEnd;
    xBegin = character.xpos;
    xEnd = character.xpos + character.width;
    yBegin = character.ypos;
    yEnd = character.ypos + character.height;
    if(obstacle.type == 0 && obstacle.xpos - chain.xpos == 20 || obstacle.xpos - chain2.xpos == 20) {
        obstacle.xpos += 100;
    }
    else if(obstacle.type != 0 && obstacle.xpos - chain.xpos == 70 || obstacle.xpos - chain2.xpos == 70) {
        obstacle.xpos += 50;
    }
    obstacle.ypos += 30;
    return (obstacle.xpos > xBegin && obstacle.xpos < xEnd && obstacle.ypos < yEnd && obstacle.ypos > yBegin);
}

bool pointInsideObject(int x, int y, Object o) {
    if(x>o.xpos && x<(o.xpos+o.width) && y>o.ypos && y<(o.ypos+o.height))
        return true;
    else
        return false;
}

void UpdateObstacles(RECT* rect) {
    if(!isPlaying)
        return;
    for(auto it = obstacles.begin(); it != obstacles.end();) {
        it->ypos -= it->dy;
        if(it->ypos+it->height < 0)
            obstacles.erase(it);
        else
            it++;
    }
}

void CheckInput(void)
{
    if(!isPlaying)
        return;
    if(keyPressed(0x41)) {  // A key
        if(yoshiCollision == -1) {
            hbmBlue = YoshiR;
            hbmBlueMask = YoshiRBlack;
        }
        else {
            hbmBlue = YoshiRT;
            hbmBlueMask = YoshiRTBlack;
        }
        yoshi.xpos = chain.xpos + 100;
    }
    else if(keyPressed(0x44)) {  // D key
        if(yoshiCollision == -1) {
            hbmBlue = YoshiL;
            hbmBlueMask = YoshiLBlack;
        }
        else {
            hbmBlue = YoshiLT;
            hbmBlueMask = YoshiLTBlack;
        }
        yoshi.xpos = chain.xpos + 125;
    }
    if(keyPressed(VK_LEFT)) {
        if(marioCollision == -1) {
            hbmRed = MarioR;
            hbmRedMask = MarioRBlack;
        }
        else {
            hbmRed = hbmMarioRT;
            hbmRedMask = hbmMarioRTBlack;
        }
        mario.xpos = chain2.xpos + 100;
    }
    else if(keyPressed(VK_RIGHT)) {
        if(marioCollision == -1) {
            hbmRed = MarioL;
            hbmRedMask = MarioLBlack;
        }
        else {
            hbmRed = hbmMarioLT;
            hbmRedMask = hbmMarioLTBlack;
        }
        mario.xpos = chain2.xpos + 125;
    }
}

void drawObstacle() {
    if(!isPlaying)
        return;
    Object obstacle;
    Object obstacle2;

    int t;
    if(rand()%3)
        t = 0;
    else
        t = rand()%3+1;

    if (yoshiGoOnFloor == false) {
    obstacle.dx = 0;
    if(yoshiCollision!=-1)
        obstacle.dy = slowedSpeed;
    else if(yoshiSpedUp != -1)
        obstacle.dy = normalSpeed + 4;
    else if(yoshiSlowDown != -1)
        obstacle.dy = normalSpeed - 4;
    else
        obstacle.dy = normalSpeed;
    obstacle.height = 100;
    obstacle.width = 100;
    obstacle.ypos = 900;
    obstacle.isMario = false;
    obstacle.collidedOnce = false;
    obstacle.type = t;
    }

    if (marioGoOnFloor == false) {
    obstacle2.dx = 0;
    if(marioCollision!=-1)
        obstacle2.dy = slowedSpeed;
    else if(marioSpedUp != -1)
        obstacle2.dy = normalSpeed + 4;
    else if(marioSlowDown != -1)
        obstacle2.dy = normalSpeed - 4;
    else
        obstacle2.dy = normalSpeed;
    obstacle2.height = 100;
    obstacle2.width = 100;
    obstacle2.ypos = 900;
    obstacle2.isMario = true;
    obstacle2.collidedOnce = false;
    obstacle2.type = t;
    }

    if(rand()%2) {
        obstacle.xpos = chain.xpos + 20;
        obstacle2.xpos = chain2.xpos + 20;
        if(obstacle.type != 0)
            obstacle.xpos += 50;
        if(obstacle2.type != 0)
            obstacle2.xpos += 50;
    }
    else {
        obstacle.xpos = chain.xpos + 20 + 150;
        obstacle2.xpos = chain2.xpos + 20 + 150;
    }

    if (yoshiGoOnFloor == false)
        obstacles.push_back(obstacle);
    if (marioGoOnFloor == false)
        obstacles.push_back(obstacle2);
}



void Draw(HWND hwnd)
{
    if(isMenu)
        drawMenu(hwnd);
    else if(isPlaying)
        drawPlaying(hwnd);
    else if(isEndGame) {
        writeInFile();
        drawEndGame(hwnd);
    }
    else if(isHighScores)
        drawHighScores(hwnd);
    else if(isInstructions)
        drawInstructions(hwnd);
}

void drawHighScores(HWND hwnd){
    RECT crect;
    HDC hdc = GetDC(hwnd);
    GetClientRect(hwnd, &crect);

    HDC hdcBuffer = CreateCompatibleDC(hdc);
    HBITMAP hbmBuffer = CreateCompatibleBitmap(hdc, crect.right, crect.bottom);
    HBITMAP hbmOldBuffer = (HBITMAP) SelectObject(hdcBuffer, hbmBuffer);

    HDC hdcMem = CreateCompatibleDC(hdc);

    HBITMAP old1=(HBITMAP)SelectObject(hdcMem, hbmHighScore);
    BitBlt(hdcBuffer, 0, -100, 900, 900, hdcMem, 0, 0, SRCCOPY);

    SelectObject(hdcMem, hbmGoBack);
    BitBlt(hdcBuffer, goBack.xpos, goBack.ypos, goBack.width, goBack.height, hdcMem, 0, 0, SRCAND);
    SelectObject(hdcMem, hbmGoBackBlack);
    BitBlt(hdcBuffer, goBack.xpos, goBack.ypos, goBack.width, goBack.height, hdcMem, 0, 0, SRCPAINT);

    SetTextColor(hdcBuffer,RGB(255,255,255));
    SelectObject(hdcBuffer, font);
    SetBkMode(hdcBuffer, TRANSPARENT);
    TextOut(hdcBuffer,220,70,"High scores",11);
    SelectObject(hdcBuffer, font2);
    for(int i=0;i<5;i++) {
        name = highScoreNames[i];
        time = highScoreTimes[i];
        try{
            time.insert (time.size()-3,1,',');
        }
        catch(std::out_of_range){
        }
        name = to_string(i+1) + " . " + name + " " + time + " s";
        TextOut(hdcBuffer,310,240+i*60,name.c_str(),name.size());
    }

    BitBlt(hdc, 0, 0, crect.right, crect.bottom, hdcBuffer, 0, 0, SRCCOPY);


    SelectObject(hdcMem, old1);
    DeleteObject(hdcMem);
    DeleteObject(hbmBuffer);

    SelectObject(hdcBuffer, hbmOldBuffer);
    DeleteObject(hdcBuffer);
    DeleteObject(hbmOldBuffer);

    ReleaseDC(hwnd, hdc);
}

void drawInstructions(HWND hwnd){
 RECT crect;
    HDC hdc = GetDC(hwnd);
    GetClientRect(hwnd, &crect);

    HDC hdcBuffer = CreateCompatibleDC(hdc);
    HBITMAP hbmBuffer = CreateCompatibleBitmap(hdc, crect.right, crect.bottom);
    HBITMAP hbmOldBuffer = (HBITMAP) SelectObject(hdcBuffer, hbmBuffer);

    HDC hdcMem = CreateCompatibleDC(hdc);

    HBITMAP old1=(HBITMAP)SelectObject(hdcMem, hbmInstructionsScreen);
    BitBlt(hdcBuffer, 0, 0, 900, 775, hdcMem, 0, 0, SRCCOPY);

    SelectObject(hdcMem, hbmGoBack);
    BitBlt(hdcBuffer, goBack.xpos, goBack.ypos, goBack.width, goBack.height, hdcMem, 0, 0, SRCAND);
    SelectObject(hdcMem, hbmGoBackBlack);
    BitBlt(hdcBuffer, goBack.xpos, goBack.ypos, goBack.width, goBack.height, hdcMem, 0, 0, SRCPAINT);

    BitBlt(hdc, 0, 0, crect.right, crect.bottom, hdcBuffer, 0, 0, SRCCOPY);


    SelectObject(hdcMem, old1);
    DeleteObject(hdcMem);
    DeleteObject(hbmBuffer);

    SelectObject(hdcBuffer, hbmOldBuffer);
    DeleteObject(hdcBuffer);
    DeleteObject(hbmOldBuffer);

    ReleaseDC(hwnd, hdc);
}

void drawEndGame(HWND hwnd) {
    RECT crect;
    HDC hdc = GetDC(hwnd);
    GetClientRect(hwnd, &crect);

    HDC hdcBuffer = CreateCompatibleDC(hdc);
    HBITMAP hbmBuffer = CreateCompatibleBitmap(hdc, crect.right, crect.bottom);
    HBITMAP hbmOldBuffer = (HBITMAP) SelectObject(hdcBuffer, hbmBuffer);

    HDC hdcMem = CreateCompatibleDC(hdc);

    HBITMAP old1=(HBITMAP)SelectObject(hdcMem, hbmEndGame);
    BitBlt(hdcBuffer, 0, 0, 900, 900, hdcMem, 0, 0, SRCCOPY);

    SelectObject(hdcMem, hbmPlayAgain);
    BitBlt(hdcBuffer, playAgain.xpos, playAgain.ypos, playAgain.width, playAgain.height, hdcMem, 0, 0, SRCAND);
    SelectObject(hdcMem, hbmPlayAgainBlack);
    BitBlt(hdcBuffer, playAgain.xpos, playAgain.ypos, playAgain.width, playAgain.height, hdcMem, 0, 0, SRCPAINT);

    SelectObject(hdcMem, hbmGoToMainMenu);
    BitBlt(hdcBuffer, gotoMainMenu.xpos, gotoMainMenu.ypos, gotoMainMenu.width, gotoMainMenu.height, hdcMem, 0, 0, SRCAND);
    SelectObject(hdcMem, hbmGoToMainMenuBlack);
    BitBlt(hdcBuffer, gotoMainMenu.xpos, gotoMainMenu.ypos, gotoMainMenu.width, gotoMainMenu.height, hdcMem, 0, 0, SRCPAINT);


    SelectObject(hdcMem, hbmYoshiStand);
    BitBlt(hdcBuffer, 100, 300, 300, 400, hdcMem, 0, 0, SRCAND);
    SelectObject(hdcMem, hbmYoshiStandBlack);
    BitBlt(hdcBuffer, 100, 300, 300, 400, hdcMem, 0, 0, SRCPAINT);

    SelectObject(hdcMem, hbmMarioStand);
    BitBlt(hdcBuffer, 500, 300, 300, 400, hdcMem, 0, 0, SRCAND);
    SelectObject(hdcMem, hbmMarioStandBlack);
    BitBlt(hdcBuffer, 500, 300, 300, 400, hdcMem, 0, 0, SRCPAINT);

    BitBlt(hdc, 0, 0, crect.right, crect.bottom, hdcBuffer, 0, 0, SRCCOPY);


    SelectObject(hdcMem, old1);
    DeleteObject(hdcMem);
    DeleteObject(hbmBuffer);

    SelectObject(hdcBuffer, hbmOldBuffer);
    DeleteObject(hdcBuffer);
    DeleteObject(hbmOldBuffer);

    ReleaseDC(hwnd, hdc);

}

void drawMenu(HWND hwnd) {
    RECT crect;
    HDC hdc = GetDC(hwnd);
    GetClientRect(hwnd, &crect);

    HDC hdcBuffer = CreateCompatibleDC(hdc);
    HBITMAP hbmBuffer = CreateCompatibleBitmap(hdc, crect.right, crect.bottom);
    HBITMAP hbmOldBuffer = (HBITMAP) SelectObject(hdcBuffer, hbmBuffer);

    HDC hdcMem = CreateCompatibleDC(hdc);

    HBITMAP old1=(HBITMAP)SelectObject(hdcMem, hbmEndGame);
    BitBlt(hdcBuffer, 0, 0, 900, 900, hdcMem, 0, 0, SRCCOPY);

    SelectObject(hdcMem, hbmPlay);
    BitBlt(hdcBuffer, newGame.xpos, newGame.ypos, newGame.width, newGame.height, hdcMem, 0, 0, SRCAND);
    SelectObject(hdcMem, hbmPlayBlack);
    BitBlt(hdcBuffer, newGame.xpos, newGame.ypos, newGame.width, newGame.height, hdcMem, 0, 0, SRCPAINT);

    SelectObject(hdcMem, hbmInstructions);
    BitBlt(hdcBuffer, instructions.xpos, instructions.ypos, instructions.width, instructions.height, hdcMem, 0, 0, SRCAND);
    SelectObject(hdcMem, hbmInstructionsBlack);
    BitBlt(hdcBuffer, instructions.xpos, instructions.ypos, instructions.width, instructions.height, hdcMem, 0, 0, SRCPAINT);

    SelectObject(hdcMem, hbmScores);
    BitBlt(hdcBuffer, scores.xpos, scores.ypos, scores.width, scores.height, hdcMem, 0, 0, SRCAND);
    SelectObject(hdcMem, hbmScoresBlack);
    BitBlt(hdcBuffer, scores.xpos, scores.ypos, scores.width, scores.height, hdcMem, 0, 0, SRCPAINT);

    SelectObject(hdcMem, hbmExit);
    BitBlt(hdcBuffer, quit.xpos, quit.ypos, quit.width, quit.height, hdcMem, 0, 0, SRCAND);
    SelectObject(hdcMem, hbmExitBlack);
    BitBlt(hdcBuffer, quit.xpos, quit.ypos, quit.width, quit.height, hdcMem, 0, 0, SRCPAINT);

    BitBlt(hdc, 0, 0, crect.right, crect.bottom, hdcBuffer, 0, 0, SRCCOPY);


    SelectObject(hdcMem, old1);
    DeleteObject(hdcMem);
    DeleteObject(hbmBuffer);

    SelectObject(hdcBuffer, hbmOldBuffer);
    DeleteObject(hdcBuffer);
    DeleteObject(hbmOldBuffer);

    ReleaseDC(hwnd, hdc);

}

void drawPlaying(HWND hwnd) {

    RECT crect;
    HDC hdc = GetDC(hwnd);
    GetClientRect(hwnd, &crect);

    HDC hdcBuffer = CreateCompatibleDC(hdc);
    HBITMAP hbmBuffer = CreateCompatibleBitmap(hdc, crect.right, crect.bottom);
    HBITMAP hbmOldBuffer = (HBITMAP) SelectObject(hdcBuffer, hbmBuffer);

    HDC hdcMem = CreateCompatibleDC(hdc);

    //left background
    HBITMAP old1=(HBITMAP)SelectObject(hdcMem, hbmBackground);
    BitBlt(hdcBuffer, 0, 0, background.width, background.height, hdcMem, 0, background.ypos, SRCCOPY);
    if (yoshiOnFloor == true) {
        SelectObject(hdcMem, hbmBackgroundFloor);
    }
    BitBlt(hdcBuffer, 0, 900-background.ypos, background.width, background.height, hdcMem, 0, 0, SRCCOPY);

    //right background
    SelectObject(hdcMem, hbmBackground);
    BitBlt(hdcBuffer, background2.width+3, 0, background2.width, background2.height, hdcMem, 0, background2.ypos, SRCCOPY);
    if (marioOnFloor == true) {
        SelectObject(hdcMem, hbmBackgroundFloor);
    }
    BitBlt(hdcBuffer, background2.width+3, 900-background2.ypos, background2.width, background2.height, hdcMem, 0, 0, SRCCOPY);

    //first chain
    SelectObject(hdcMem, hbmChain);
    BitBlt(hdcBuffer, chain.xpos, chain.ypos, chain.width, chain.height, hdcMem, 0, 0, SRCAND);
    HBITMAP old=(HBITMAP)SelectObject(hdcMem, hbmChainMask);
    BitBlt(hdcBuffer, chain.xpos, chain.ypos, chain.width, chain.height, hdcMem,0, 0, SRCPAINT);

    //second chain
    SelectObject(hdcMem, hbmChain);
    BitBlt(hdcBuffer, chain2.xpos, chain2.ypos, chain2.width, chain2.height, hdcMem, 0, 0, SRCAND);
    SelectObject(hdcMem, hbmChainMask);
    BitBlt(hdcBuffer, chain2.xpos, chain2.ypos, chain2.width, chain2.height, hdcMem,0, 0, SRCPAINT);

    //yoshi
    SelectObject(hdcMem, hbmBlue);
    BitBlt(hdcBuffer, yoshi.xpos, yoshi.ypos, yoshi.width, yoshi.height, hdcMem, 0, 0, SRCAND);
    SelectObject(hdcMem, hbmBlueMask);
    BitBlt(hdcBuffer, yoshi.xpos, yoshi.ypos, yoshi.width, yoshi.height, hdcMem,0, 0, SRCPAINT);

    //mario
    SelectObject(hdcMem, hbmRed);
    BitBlt(hdcBuffer, mario.xpos, mario.ypos, mario.width, mario.height, hdcMem, 0, 0, SRCAND);
    SelectObject(hdcMem, hbmRedMask);
    BitBlt(hdcBuffer, mario.xpos, mario.ypos, mario.width, mario.height, hdcMem,0, 0, SRCPAINT);

    //obstacles

    for(int i=0; i<obstacles.size(); i++) {
        if(obstacles.at(i).type == 0) {
            SelectObject(hdcMem, hbmObstacle);
            BitBlt(hdcBuffer, obstacles.at(i).xpos, obstacles.at(i).ypos, obstacles.at(i).width, obstacles.at(i).height, hdcMem, 0, 0, SRCAND);
            SelectObject(hdcMem, hbmObstacleMask);
            BitBlt(hdcBuffer, obstacles.at(i).xpos, obstacles.at(i).ypos, obstacles.at(i).width, obstacles.at(i).height, hdcMem, 0, 0, SRCPAINT);
        }
        else if(obstacles.at(i).type == 1) {
            SelectObject(hdcMem, hbmSpeed);
            BitBlt(hdcBuffer, obstacles.at(i).xpos, obstacles.at(i).ypos, obstacles.at(i).width, obstacles.at(i).height, hdcMem, 0, 0, SRCAND);
            SelectObject(hdcMem, hbmSpeedBlack);
            BitBlt(hdcBuffer, obstacles.at(i).xpos, obstacles.at(i).ypos, obstacles.at(i).width, obstacles.at(i).height, hdcMem, 0, 0, SRCPAINT);
        }
        else if(obstacles.at(i).type == 2) {
            SelectObject(hdcMem, hbmSlow);
            BitBlt(hdcBuffer, obstacles.at(i).xpos, obstacles.at(i).ypos, obstacles.at(i).width, obstacles.at(i).height, hdcMem, 0, 0, SRCAND);
            SelectObject(hdcMem, hbmSlowBlack);
            BitBlt(hdcBuffer, obstacles.at(i).xpos, obstacles.at(i).ypos, obstacles.at(i).width, obstacles.at(i).height, hdcMem, 0, 0, SRCPAINT);
        }
        else if(obstacles.at(i).type == 3) {
            SelectObject(hdcMem, hbmSwitch);
            BitBlt(hdcBuffer, obstacles.at(i).xpos, obstacles.at(i).ypos, obstacles.at(i).width, obstacles.at(i).height, hdcMem, 0, 0, SRCAND);
            SelectObject(hdcMem, hbmSwitchBlack);
            BitBlt(hdcBuffer, obstacles.at(i).xpos, obstacles.at(i).ypos, obstacles.at(i).width, obstacles.at(i).height, hdcMem, 0, 0, SRCPAINT);
        }

        if(obstacles.at(i).type == 0 && (!marioImortal || obstacles.at(i).collidedOnce) && overlapping(obstacles.at(i), mario)) {
            for(int k=0;k<5;k++){
                SelectObject(hdcMem,hbmexplosion);
                BitBlt(hdcBuffer,obstacles.at(i).xpos-6, obstacles.at(i).ypos-6,explosionWidth,explosionHeight,hdcMem,k*explosionWidth,2*explosionHeight,SRCAND);
                SelectObject(hdcMem,hbmexplosionblack);
                BitBlt(hdcBuffer,obstacles.at(i).xpos-6, obstacles.at(i).ypos-6,explosionWidth,explosionHeight,hdcMem,k*explosionWidth,2*explosionHeight,SRCPAINT);
            }
        }

        if(obstacles.at(i).type == 0 && (!yoshiImortal || obstacles.at(i).collidedOnce) && overlapping(obstacles.at(i), yoshi)) {
            for(int k=0;k<5;k++){
                SelectObject(hdcMem,hbmexplosion);
                BitBlt(hdcBuffer,obstacles.at(i).xpos-6, obstacles.at(i).ypos-6,explosionWidth,explosionHeight,hdcMem,k*explosionWidth,2*explosionHeight,SRCAND);
                SelectObject(hdcMem,hbmexplosionblack);
                BitBlt(hdcBuffer,obstacles.at(i).xpos-6, obstacles.at(i).ypos-6,explosionWidth,explosionHeight,hdcMem,k*explosionWidth,2*explosionHeight,SRCPAINT);
            }
        }
    }
    BitBlt(hdc, 0, 0, crect.right, crect.bottom, hdcBuffer, 0, 0, SRCCOPY);


    SelectObject(hdcMem, old1);
    DeleteObject(hdcMem);
    DeleteObject(hbmBuffer);

    SelectObject(hdcBuffer, hbmOldBuffer);
    DeleteObject(hdcBuffer);
    DeleteObject(hbmOldBuffer);

    ReleaseDC(hwnd, hdc);
}

void startNewGame() {
    mciSendString("play sound/theme_music.wav", NULL, 0, NULL);
    Initialize();
    startTime = GetTickCount();

    floor = 70000;
    mario_to_floor = 0;
    yoshi_to_floor = 0;

    onceMario = true;
    onceYoshi = true;
    isMarioWinner = false;
    marioSlowed = false;
    yoshiSlowed = false;
    marioImortal = false;
    yoshiImortal = false;
    marioOnFloor = false;
    yoshiOnFloor = false;
    marioGoOnFloor = false;
    yoshiGoOnFloor = false;
    marioCollision = -1;
    yoshiCollision = -1;
    marioSpedUp = -1;
    marioSlowDown = -1;
    yoshiSpedUp = -1;
    yoshiSlowDown = -1;
    obstacles.clear();

    isMenu = false;
    isEndGame = false;
    isHighScores = false;
    isPlaying = true;
}

void showScores() {
    isMenu = false;
    isEndGame = false;
    isPlaying = false;
    isInstructions = false;
    isHighScores = true;
}

void showInstructions() {
    isMenu = false;
    isEndGame = false;
    isPlaying = false;
    isHighScores = false;
    isInstructions = true;
}












